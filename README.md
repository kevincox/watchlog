# watchlog

Do you ever have a terminal watching logs or recompiling on save and you aren't sure if the most recent messages are from the change you just made, or 10 minutes ago? watchlog will help you but adding a message at the bottom of your terminal that haven't produced output recently. After a short period of no output watchlog will start displaying the time since last output below the command.

<pre>HTTP/1.1 GET /api/books/1 500
HTTP/1.1 GET /api/books/2 500
HTTP/1.1 GET /api/books/1 500
HTTP/1.1 GET /api/books/1 200
<font color="#8D9197">Last output 48s ago.</font>
</pre>

When the command produces more output it will look like normal if it hasn't been too long, however if it has been a while watchlog will inject a timestamp so that the logs are grouped by when they occurred when you scroll up.

[If you still don't get it I have a longer explanation of how I use watchlog on my blog.](https://kevincox.ca/2021/04/21/announcing-watchlog/)

## Demo

A demo at 6x speed.

![demo of watchlog](doc/media/demo.gif)

## Usage

Simply put `| wl` after your command.

```sh
$ python -m http.server | wl
```

## Configuration

watchlog can be configured with `watchlog/config.scfg`. This file will be read from all of the appropriate places for your platform. For example on a Linux system it will be looked for in `~/.config/watchlog` and `/etc/xdg/watchlog` (or whatever you have configured in `$XDG_CONFIG_DIRS`). If multiple files are found they will be merged according to precedence. If a value is not set in any config file a builtin default will be used.

The following options are supported, the first example for each option shows the default value.

### Options

#### delay ([Duration](#duration))

Enable watchlog when there is no output for this long.

```yaml
delay: 3s
```

#### permanent-delay ([Option\<Duration>](#optionduration))

Insert a permanent timestamp into the output after a delay of at least this long.

Note that a timestamp is never inserted after pauses which occur mid-line.

```yaml
permanent-delay: 10s
```
```yaml
permanent-delay: never
```

### Types

#### Duration

Durations can be specified in a simple form with units.

```yaml
delay: 10s
permanent-delay: 1h 2m 30s
```

#### Option\<Duration>

Optional durations can be set to `never` for an infinite duration or a [Duration](#Duration) value.

```
permanent-delay: never
```

## Installation

### [Cargo](https://doc.rust-lang.org/cargo/)

```sh
$ cargo install watchlog
```

### [Nix](https://nixos.org)

[watchlog is available in nixpkgs](https://search.nixos.org/packages?channel=unstable&show=watchlog&type=packages)

Or you can build from source:

```sh
$ nix-env -i -f https://gitlab.com/kevincox/watchlog/-/archive/v1/watchlog-v1.tar.bz2 -A watchlog
```

## Questions, Comments or Feedback

I am happy to answer any questions, discuss features, bugs or possible contributions. Just [open an issue](https://gitlab.com/kevincox/watchlog/-/issues/new) (even if you just want to say hello).
